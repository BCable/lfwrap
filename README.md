lfwrap v1.1 Documentation
=========================

Table of Contents
-----------------

    Introduction

    I: Directory Structure

    II: Commands

      II.a: Standard Commands
      II.b: Windows Only Commands

    III: Config

      III.a: lfwrap.conf
      III.b: sites.conf

    IV: Usage on Windows

      IV.a: Quick Start (Windows)
      IV.b: Uninstallation (Windows)


Introduction
------------

LFWrap is an executable wrapper for creating DreamWeaver style lock files for
other editors.  LFWrap is written in C++, and is cross platform (Windows and
Linux tested).

License: GPLv2


I. Directory Structure
----------------------

    batch/        - Directory containing the old batch files that this evolved from.
                    Really hacky and old, do not use it is crap.

    bin/          - Configuration files and Win32 executable version of the
                    application.

    src/          - Application source code.

    README.md     - This file.


II. Commands
------------

    lfwrap <command> <file/ext>

Argument descriptions:

    dir  - A directory to do the command on.

    file - A filename to do the command on.

    ext  - A file extension to do the command on.


II.a: Standard Commands
-----------------------

    -v                - Display version information and quit.

    checkin <file>    - Checks for the existence of a DreamWeaver lock file, then
                        checks the file in if you are the owner of the lock.
                        Otherwise, a prompt is shown displaying who really owns the
                        lock, and if you wish to override their lock.

    checkin_dir <dir> - Recursively checks for any DreamWeaver lock files within the
                        specified directory, and checks them in if necessary.
                        Provides prompts for lock files checked out by other users.

    checkout <file>   - Checks out the file as yourself, if the file is not already
                        checked out.

    edit <file>       - Checks out the file, then executes the editor defined in
                        lfwrap.conf.  If the editor is already running, then the
                        set of options used for the second editor is used as defined
                        in lfwrap.conf.

    get <file>        - Retrieves the most recent modified date of all files
                        associated with a file, then overwrites the current file
                        with the newest file associated with it.

    put <file>        - Copies a file's contents to all other associated files as
                        defined by sites.conf.

    stat <file>       - Prints information about a file, such as if the file is
                        checked out, who has the file checked out, and any other
                        files linked to that file through sites.conf.


II.b: Windows Only Commands
---------------------------

    install_shell <ext>   - Install the shell extensions for the specified file
                            extension.  Please note, these shell extensions rely on
                            the current location of the lfwrap.exe file, which
                            means if you move this file, you must uninstall, then
                            reinstall these shell extensions.

    install_shell_dir     - Install the shell extensions for all directories.
                            Please note, these shell extensions rely on the current
                            location of the lfwrap.exe file, which means if you move
                            this file, you must uninstall, then reinstall these
                            shell extensions.

    uninstall_shell <ext> - Uninstall the shell extensions for the specified file
                            extension.

    uninstall_shell_dir   - Uninstall the shell extensions for all directories.


III: Config
-----------

III.a: lfwrap.conf
------------------

    LFW_USER              - Name of user to use when checking out files.

    LFW_EMAIL             - Email of user to use when checking out files.

    LFW_CONFIG_DIR        - Directory that stores configuration files.

    LFW_CONFIG_SITES      - "sites.conf" filename.  Just leave as "sites.conf".

    LFW_STATUS_DIR        - Directory that stores status files.  Same directory as
                            LFW_CONFIG_DIR works fine.

    LFW_EDITOR            - Path to editor you wish to execute when using the "edit"
                            command.  On UNIX systems, if editor is in PATH
                            environment variable, the binary name is all that's
                            necessary.

    LFW_EDITOR_OPTS       - Options to pass to the LFW_EDITOR.

    LFW_EDITORSECOND      - Same as LFW_EDITOR, but if the editor is already
                            running, this is the file to execute instead.  Usually
                            the same as LFW_EDITOR.

    LFW_EDITORSECOND_OPTS - Options to pass to the LFW_EDITORSECOND.


III.b: sites.conf
----------------

Each line of the sites.conf file specifies a set of directories to be linked
together as if they were one project.  For instance:

    C:\project = C:\project-debug = C:\project-test

Would make all files and directories under these files to be linked together
when creating and removing lock files.

If C:\project\app.cpp, C:\project-debug\app.cpp, or C:\project-test.cpp were to
be checked out, then the following files would be created:

    C:\project\app.cpp.LCK
    C:\project-debug\app.cpp.LCK
    C:\project-test\app.cpp.LCK

Likewise, those files would be removed if any of those files were checked back
in.  This provides the same functionality as DreamWeaver's site management
features.

Any number of directories can be linked together, so long as they are all on
the same line.


IV. Usage on Windows
---------------------------

IV.a: Quick Start (Windows)
---------------------------

1. Extract all files to C:\lfwrap
2. Edit C:\lfwrap\bin\lfwrap.conf
3. Change LFW_USER and LFW_EMAIL accordingly.
4. Change LFW_CONFIG_DIR and LFW_STATUS_DIR to C:\lfwrap\bin
5. Uncomment one of the editor lines, and rewrite the path to the binary file.
6. Open up a Command Prompt (Start -> Run -> cmd).
7. Type: C:\lfwrap\bin\lfwrap.exe install_shell_dir
8. Type: C:\lfwrap\bin\lfwrap.exe install_shell php
9. Now all PHP files are associated with lfwrap, repeat for files of other
   types.

IV.b: Uninstallation (Windows)
------------------------------

1. Open up a Command Prompt (Start -> Run -> cmd).
2. Type: C:\lfwrap\bin\lfwrap.exe uninstall_shell_dir
3. Type: C:\lfwrap\bin\lfwrap.exe uninstall_shell php
4. Repeat for files of other types you installed the program for.
5. Delete C:\lfwrap
