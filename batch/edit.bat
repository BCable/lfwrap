@ECHO OFF
cd /d %0\..
call config.bat

setlocal ENABLEDELAYEDEXPANSION
set bypass=0
if '%~1'=='-b' (
	set bypass=1
	shift
)

call checkout.bat %1

if "%success%"=="0" goto end

if exist "%LFW_EDITOROPENFILE%" goto editoropen

title LFWRAP
echo 1 > "%LFW_EDITOROPENFILE%"
call hide.bat
if '!bypass!'=='0' start /w "LFW_EDITOR" "%LFW_EDITOR%" %LFW_EDITOR_OPTS% %1
del "%LFW_EDITOROPENFILE%"
goto end

:editoropen
if '!bypass!'=='0' start /w "LFW_EDITORSECOND" "%LFW_EDITORSECOND%" %LFW_EDITORSECOND_OPTS% %1
goto end


:end
endlocal