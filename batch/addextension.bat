@ECHO OFF
cd /d $0\..

echo REGEDIT4 > $openwith.reg

echo [HKEY_CLASSES_ROOT\Applications\edit.bat\shell\open\command] >> $openwith.reg
echo @="\"%~1\\edit.bat\" \"%%1\"" >> $openwith.reg

echo [HKEY_CLASSES_ROOT\Applications\checkout.bat\shell\open\command] >> $openwith.reg
echo @="\"%~1\\checkout.bat\" \"%%1\"" >> $openwith.reg

echo [HKEY_CLASSES_ROOT\Applications\checkin.bat\shell\open\command] >> $openwith.reg
echo @="\"%~1\\checkin.bat\" \"%%1\"" >> $openwith.reg

echo [HKEY_CLASSES_ROOT\Applications\clear_locks.bat\shell\open\command] >> $openwith.reg
echo @="\"%~1\\clear_locks.bat\" \"%%1\"" >> $openwith.reg

echo [HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.%~2] >> $openwith.reg
echo "Application"="edit.bat" >> $openwith.reg

echo [HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.%~2\OpenWithList] >> $openwith.reg
echo "a"="edit.bat" >> $openwith.reg
echo "b"="checkout.bat" >> $openwith.reg
echo "c"="checkin.bat" >> $openwith.reg
echo "d"="clear_locks.bat" >> $openwith.reg
echo "MRUList"="abcd" >> $openwith.reg

regedit /s $openwith.reg
del $openwith.reg