@ECHO OFF
cd /d $0\..

call get_escaped_slashes.bat "%CD%"

set /p choice=Enter a file extension (NO PERIOD) to associate LFWrap with: 
if not "%choice%"=="" (
	call addextension.bat "%ges_result%" "%choice%"
	goto success
)
goto fail

:success
echo Success
goto pause_end

:fail
echo Failed
goto pause_end

:pause_end
pause
:end