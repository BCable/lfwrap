@ECHO OFF
cd /d %0\..
call config.bat

set success=0
set sessionopen=0

if exist %1.LCK goto checkedout
if exist %LFW_LOCKSFILE% goto append

:new
echo %~1 > %LFW_LOCKSFILE%
echo %LFW_USER% > %1.LCK
goto success

:append
set sessionopen=1
echo %~1 >> %LFW_LOCKSFILE%
echo %LFW_USER% > %1.LCK
goto success

:success
set success=1
goto end

:checkedout
call get_co_user.bat %1
if %result%==%LFW_USER% (
	set success=1
	goto end
)
if not %result%==%LFW_USER% goto fail_checkedout

:fail_checkedout
echo File already checked out by '%result%': %~1
goto pause_end

:pause_end
pause
:end
