@ECHO OFF
cd /d %0\..
call config.bat

setlocal ENABLEDELAYEDEXPANSION
set gsi_result=

for /f %%l in (%SITE_CONFIG_FILE%) do (

	set continue=1
	for /l %%i in (1,1,10) do if '!continue!'=='1' (
		set i=%%i

		set result=
		call get_delim.bat "%%l" "*" !i!
		if '!result!'=='' set continue=0

		if '!continue!'=='1' (
			find !result!
			echo !result! %~1
		)
	)

)
:end

endlocal & set gsi_result=%gsi_result%
