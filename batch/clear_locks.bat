@ECHO OFF
cd /d %0\..
call config.bat

if not exist "%LFW_LOCKSFILE%" goto end

for /f %%f in (%LFW_LOCKSFILE%) do if exist "%%f.LCK" del "%%f.LCK"
if exist "%LFW_LOCKSFILE%" del "%LFW_LOCKSFILE%"
goto end

:end