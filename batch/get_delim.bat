@ECHO OFF

set string="%~1"
set delims=%~2
set tokens=%~3

for /f "delims=%delims% tokens=%tokens%" %%s in (%string%) do set result=%%s
