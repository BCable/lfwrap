@ECHO OFF

setlocal ENABLEDELAYEDEXPANSION
echo %~1 > $get_escaped_slashes_tmp
set ges_result=
for /l %%i in (0,1,1000) do (
	set i=%%i
	call get_substr.bat %~1 !i! 1
	if '!gs_result!'=='' goto break
	if '!i!'=='0' set ges_result=!gs_result!
	if not '!i!'=='0' (
		if '!gs_result!'=='\' set ges_result=!ges_result!\\
		if not '!gs_result!'=='\' set ges_result=!ges_result!!gs_result!
	)
)
:break
del $get_escaped_slashes_tmp

endlocal & set ges_result=%ges_result%
