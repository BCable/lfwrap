@ECHO OFF
cd /d %0\..
call config.bat

if not exist "%~1.LCK" goto fail_notcheckedout

echo 1
call get_co_user.bat %1
echo 2
if "%result%"=="%LFW_USER%" goto checkin
echo asd
goto checkedout_notuser

:checkedout_notuser
echo This file is checked out by '%result%': %~1
set /p choice=Do you want to check in anyway (Y/N)? 
if not "%choice%"=="" set choice=%choice:~0,1%
if "%choice%"=="Y" goto checkin
if "%choice%"=="y" goto checkin
goto end

:checkin
type "%LFW_LOCKSFILE%" | find /v "%~1" > "%LFW_LOCKSFILE%.tmp"
del "%LFW_LOCKSFILE%"
move "%LFW_LOCKSFILE%.tmp" "%LFW_LOCKSFILE%"
del "%~1.LCK"
goto success

:success
goto end

:fail_notcheckedout
echo File not checked out: %~1
goto pause_end

:pause_end
pause
:end
