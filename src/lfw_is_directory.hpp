#ifndef _lfwrap_is_directory_hpp_
#define _lfwrap_is_directory_hpp_

#include <string>

bool is_directory(std::string);

#endif
