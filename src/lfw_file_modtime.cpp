#include "lfw_file_modtime.hpp"

#include <sys/stat.h>

using namespace std;

#ifdef LFW_GNU
time_t file_modtime(string filename){
	struct stat info;

	if(stat(filename.c_str(), &info)==0)
		return info.st_mtime;
	else
		return 0;
}
#else
time_t file_modtime(string filename){
	struct _stat64i32 info;

	if(_stat(filename.c_str(), &info)==0)
		return info.st_mtime;
	else
		return 0;
}
#endif
