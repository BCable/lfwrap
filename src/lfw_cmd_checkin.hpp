#ifndef _lfwrap_cmd_checkin_hpp_
#define _lfwrap_cmd_checkin_hpp_

#include "Config.hpp"
#include "DWFile.hpp"

void cmd_checkin(Config*, DWFile*);

#endif
