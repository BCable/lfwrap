#ifndef _lfwrap_file_modtime_hpp_
#define _lfwrap_file_modtime_hpp_

#include "globals.hpp"

#include <string>

#include <sys/types.h>

time_t file_modtime(std::string);

#endif
