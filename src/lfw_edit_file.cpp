#include "lfw_edit_file.hpp"

#include "globals.hpp"
#include "lfw_file_exists.hpp"

#include <stdio.h>

#ifdef LFW_WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

using namespace std;

void edit_file(Config* lc, string filename){
	string cmd;
	string status_file;
	bool do_statusfile;

	if(!file_exists(
		lc->getValueOf("LFW_STATUS_DIR")+
		LFW_FILESYSTEM_SLASH+
		"LFW_EDITOROPEN"
	)){
		cmd=
			lc->getValueOf("LFW_EDITOR")+" "+
			lc->getValueOf("LFW_EDITOR_OPTS")+
			" \""+filename+"\"";
		do_statusfile=true;
	}

	else{
		cmd=
			lc->getValueOf("LFW_EDITORSECOND")+" "+
			lc->getValueOf("LFW_EDITORSECOND_OPTS")+
			" \""+filename+"\"";
		do_statusfile=false;
	}

	// create status file
	if(do_statusfile){
		status_file=
			lc->getValueOf("LFW_STATUS_DIR")+
			LFW_FILESYSTEM_SLASH+
			"LFW_EDITOROPEN";
		FILE* fp;
		fp=fopen(status_file.c_str(), "w");
		fclose(fp);
	}

	// execute command
#ifdef LFW_WIN32
	HWND hWnd=GetConsoleWindow();
	ShowWindow(hWnd, SW_HIDE);
#endif
	system(cmd.c_str());
#ifdef LFW_WIN32
	ShowWindow(hWnd, SW_SHOW);
#endif

	// delete status file
	if(do_statusfile){
#ifdef LFW_WIN32
		_unlink(status_file.c_str());
#else
		unlink(status_file.c_str());
#endif
	}

}
