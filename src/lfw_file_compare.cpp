#include "lfw_file_compare.hpp"

#include "globals.hpp"

#include <fstream>

using namespace std;

bool file_compare(string file1, string file2){
	string str1;
	string str2;

	ifstream in1 (file1.c_str());
	ifstream in2 (file2.c_str());

	in1 >> str1;
	in2 >> str2;

	return (str1.compare(str2)==0);
}
