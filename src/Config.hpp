#ifndef _lfwrap_config_hpp_
#define _lfwrap_config_hpp_

#include <string>
#include <vector>

class Config {
	private:
		// constructor method
		void init(const char*);

		std::vector<std::string> config[2];
		short                    error;
		int                      getKeyPos(std::string);

	public:
		Config(const char*);
		Config(std::string);

		// errors
		static const short ERRORNONE=0;
		static const short FILENOTFOUND=1;

		void        appendValue(std::string,std::string);
		short       getError();
		std::string getValueOf(std::string);
		bool        keyExists(std::string);
};

#endif
