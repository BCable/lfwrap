#ifndef _lfwrap_string_hpp_
#define _lfwrap_string_hpp_

#include "lfw_string.hpp"

using namespace std;

string string_lower(string str){
	string::size_type i;
	for(i=0; i<str.length(); i++){
		str[i]=tolower(str[i]);
	}
	return str;
}

string string_strip(string str){
	string::size_type start, end;
	for(start=0; start<str.length(); start++){
		if(
			str[start]!=' ' &&
			str[start]!='\t' &&
			str[start]!='\r' &&
			str[start]!='\n'
		) break;
	}
	for(end=str.length()-1; end>start; end--){
		if(
			str[end]!=' ' &&
			str[end]!='\t' &&
			str[end]!='\r' &&
			str[end]!='\n'
		) break;
	}
	return str.substr(start,end-start+1);
}

#endif
