#include "Config.hpp"

#include <fstream>

#include "lfw_string.hpp"

using namespace std;

// constructors {{{

Config::Config(const char* filename){
	init(filename);
}

Config::Config(string filename){
	init(filename.c_str());
}

void Config::init(const char* filename){
	ifstream config_file (filename);
	string line;
	int splitter;
	string name;
	string value;

	if(!config_file.is_open()){
		error=FILENOTFOUND;
		return;
	}

	while(!config_file.eof()){
		getline(config_file, line);

		splitter=line.find_first_of('=');
		if(splitter==-1)
			continue;

		name=string_strip(line.substr(0,splitter));
		value=string_strip(line.substr(splitter+1));

		this->appendValue(name,value);

	}

	config_file.close();

	error=ERRORNONE;
}

// }}}

// private {{{

int Config::getKeyPos(string key){
	vector<string>::size_type i;
	key=string_lower(key);

	for(i=0; i<config[0].size(); i++){
		if(key.compare(config[0].at(i))==0)
			break;
	}

	if(i==config[0].size())
		return -1;
	else
		return i;
}

// }}}

// public {{{

void Config::appendValue(string key, string val){
	key=string_lower(key);

	config[0].push_back(key);
	config[1].push_back(val);
}

short Config::getError(){
	return error;
}

string Config::getValueOf(string key){
	int pos=getKeyPos(key);
	if(pos==-1)
		return "";
	else
		return config[1].at(pos);
}

bool Config::keyExists(string key){
	if(getKeyPos(key)==-1)
		return false;
	else
		return true;
}

// }}}
