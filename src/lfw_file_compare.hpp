#ifndef _lfwrap_file_compare_hpp
#define _lfwrap_file_compare_hpp

#include <string>

bool file_compare(std::string, std::string);

#endif
