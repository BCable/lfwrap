#include "lfw_cmd_checkin.hpp"

#include "lfw_uni_exit.hpp"

#include <iostream>

using namespace std;

void cmd_checkin(Config* lc, DWFile* dwf){
	if(dwf->isCheckedOut()){
		if(dwf->getOwnerName().compare(lc->getValueOf("LFW_USER"))!=0){
			cout << "File is checked out by '" << dwf->getOwnerName();
			cout << "', checkin anyway? ";

			char c;
			cin >> c;
			if(c!='y' && c!='Y'){
				cout << "Quitting." << endl;
				uni_exit(0);
			}
		}
		dwf->checkIn();
	}
	else
		cout << "File is already checked in." << endl;
}
