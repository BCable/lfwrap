#ifndef _lfwrap_globals_hpp_
#define _lfwrap_globals_hpp_

#define VERSION "1.1"

#define LFW_ABSOLUTE 0
#define LFW_RELATIVE_APP 1
#define LFW_RELATIVE_PWD 2

#define LFW_CONFIG_FILE "lfwrap.conf"
#define LFW_CONFIG_TYPE LFW_RELATIVE_APP

#ifdef _WIN32
#  ifdef __CYGWIN__
#    define LFW_FILESYSTEM_SLASH '/'
#  else
#    define LFW_FILESYSTEM_SLASH '\\'
#  endif
#  ifdef __GNUC__
#    define LFW_GNU
#  else
#    define LFW_WIN32
//   for hiding console window
#    define _WIN32_WINNT 0x0500
#    define _CRT_SECURE_NO_WARNINGS 1
#  endif
#else
#  define LFW_GNU
#  define LFW_FILESYSTEM_SLASH '/'
#  define LFW_CASE_SENSITIVE
#endif

#define LFW_LOCKFILE_SUFFIX ".LCK"
#define LFW_LOCKFILE_DELIM "||"

// messages
#define MSG_ERROR_UNKNOWN "An unknown error occurred."
#define MSG_ERROR_FILENOTFOUND "File not found."
#define MSG_ERROR_ISDIRECTORY "The path specified is a directory."
#define MSG_ERROR_BUFOVERFLOW "Attempted buffer overflow detected."

#endif
