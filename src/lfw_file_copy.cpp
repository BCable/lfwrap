#include "lfw_file_copy.hpp"

#include "globals.hpp"

#include "lfw_string.hpp"

#include <fstream>

using namespace std;

void file_copy(string infile, string outfile){
#ifndef LFW_CASE_SENSITIVE
	string lowerinfile;
	string loweroutfile;
	lowerinfile=string_lower(infile);
	loweroutfile=string_lower(outfile);
	if(lowerinfile.compare(loweroutfile)==0) return;
#else
	if(infile.compare(outfile)==0) return;
#endif

	remove(outfile.c_str());
	ifstream in (infile.c_str());
	ofstream out (outfile.c_str());
	out << in.rdbuf();
	out.close();
	in.close();
}
