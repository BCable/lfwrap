
// WINDOWS ONLY CODE
#include "lfw_shellext.hpp"

#include "globals.hpp"
#include <windows.h>
#include <string>
#include <string.h>
#include <tchar.h>
#include <iostream>

using namespace std;

// string helper functions {{{

void shellext_create(TCHAR* buf, DWORD size, TCHAR* one, TCHAR* two){
	if(wcslen(one)+1>size){
		cerr << MSG_ERROR_BUFOVERFLOW << endl;
		exit(1);
	}

	wcscpy(buf, one);

	if(wcslen(buf)+wcslen(two)+1>size){
		cerr << MSG_ERROR_BUFOVERFLOW << endl;
		exit(1);
	}

	wcscat(buf, two);
}

void shellext_create(
	TCHAR* buf, DWORD size, TCHAR* one, TCHAR* two, TCHAR* three
){
	shellext_create(buf, size, one, two);

	if(wcslen(buf)+wcslen(three)+1>size){
		cerr << MSG_ERROR_BUFOVERFLOW << endl;
		exit(1);
	}

	wcscat(buf, three);
}

void shellext_strtolower(char* cstr){
	size_t i;
	for(i=0; i<strlen(cstr); i++){
		cstr[i]=tolower(cstr[i]);
	}
}

// }}}

// registry helper functions {{{

void shellext_store(
	HKEY root_hkey, TCHAR* key, TCHAR* name, TCHAR* value
){
	DWORD dwDisp;
	DWORD dwSize;
	HKEY  hkey;
	long  ret;

	ret=RegCreateKeyEx(
		root_hkey, key, 0, NULL, 0, KEY_ALL_ACCESS, NULL, &hkey, &dwDisp
	);

	if(ret==ERROR_SUCCESS){
		dwSize=sizeof(TCHAR)*(wcslen(value));

		ret=RegSetValueEx(hkey, name, 0, REG_SZ, (PBYTE) value, dwSize);

		if(ret!=ERROR_SUCCESS){
			cerr << "Failed writing registry value.  Error number: ";
			cerr << ret << endl;
			exit(1);
		}

		RegCloseKey(hkey);
	}
	else{
		cerr << "Failed opening registry key.  Error number: " << ret << endl;
		exit(1);
	}
}

TCHAR* shellext_query(HKEY root_hkey, TCHAR* key, TCHAR* name){
	DWORD  dwDisp;
	DWORD  dwSize;
	DWORD  dwType;
	HKEY   hkey;
	long   ret;
	TCHAR* value;
	
	ret=RegCreateKeyEx(
		root_hkey, key, 0, NULL, 0, KEY_ALL_ACCESS, NULL, &hkey, &dwDisp
	);

	if(ret==ERROR_SUCCESS){
		dwType=REG_SZ;

		ret=RegQueryValueEx(hkey, name, NULL, &dwType, NULL, &dwSize);
		if(ret!=ERROR_SUCCESS){
			cerr << "Failed retrieving registry value length.  Error number: ";
			cerr << ret << endl;
			exit(1);
		}

		value=(TCHAR*) malloc(sizeof(TCHAR)*(dwSize+1));
		value[dwSize]=0;

		ret=RegQueryValueEx(hkey, name, NULL, &dwType, (PBYTE) value, &dwSize);
		if(ret!=ERROR_SUCCESS){
			cerr << "Failed retrieving registry value.  Error number: ";
			cerr << ret << endl;
			exit(1);
		}

		return value;
	}
	else{
		cerr << "Failed opening registry key.  Error number: " << ret << endl;
		exit(1);
	}
}

// }}}

// register open shell extension {{{

void shellext_register(const char* file_ext){
	TCHAR buf[MAX_PATH];
	TCHAR path[MAX_PATH];
	TCHAR wfile_ext[MAX_PATH];

	// path to executable
	GetModuleFileName(NULL, path, MAX_PATH);

	// define 'lfwrap.exe' application
	shellext_create(buf, MAX_PATH, L"\"", path, L"\" edit \"%1\"");
	shellext_store(
		HKEY_CLASSES_ROOT,
		L"Applications\\lfwrap.exe\\shell\\open\\command",
		NULL, buf
	);

	// associate 'open' command with 'lfwrap.exe'
	MultiByteToWideChar(
		CP_ACP, MB_PRECOMPOSED,
		file_ext, strlen(file_ext)+1,
		wfile_ext, MAX_PATH
	);
	shellext_create(buf, MAX_PATH,
		L"Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\.",
		wfile_ext
	);
	shellext_store(HKEY_CURRENT_USER, buf, L"Application", L"lfwrap.exe");
}

// }}}

// install all shell extensions for filetype {{{

void shellext_install(const char* file_ext){
	shellext_strtolower((char*) file_ext);

	shellext_register(file_ext);

	TCHAR* filetype_classname;
	TCHAR  key[MAX_PATH];
	TCHAR  path[MAX_PATH];
	TCHAR  value[MAX_PATH];
	TCHAR  wfile_ext[MAX_PATH];

	// path to executable
	GetModuleFileName(NULL, path, MAX_PATH);

	// wide file extension
	MultiByteToWideChar(
		CP_ACP, MB_PRECOMPOSED,
		file_ext, strlen(file_ext)+1,
		wfile_ext, MAX_PATH
	);

	// get full filetype class name from registry
	shellext_create(key, MAX_PATH, L".", wfile_ext);
	filetype_classname=shellext_query(HKEY_CLASSES_ROOT, key, NULL);

	// create Checkin shell extension
	shellext_create(
		key, MAX_PATH, filetype_classname, L"\\shell\\LFW - Checkin"
	);
	shellext_store(HKEY_CLASSES_ROOT, key, NULL, L"LFW - Checkin");

	shellext_create(key, MAX_PATH,
		filetype_classname, L"\\shell\\LFW - Checkin\\command"
	);
	shellext_create(value, MAX_PATH, L"\"", path, L"\" checkin \"%1\"");
	shellext_store(HKEY_CLASSES_ROOT, key, NULL, value);

	// create Checkout shell extension
	shellext_create(
		key, MAX_PATH, filetype_classname, L"\\shell\\LFW - Checkout"
	);
	shellext_store(HKEY_CLASSES_ROOT, key, NULL, L"LFW - Checkout");

	shellext_create(key, MAX_PATH,
		filetype_classname, L"\\shell\\LFW - Checkout\\command"
	);
	shellext_create(value, MAX_PATH, L"\"", path, L"\" checkout \"%1\"");
	shellext_store(HKEY_CLASSES_ROOT, key, NULL, value);

	// create Edit shell extension
	shellext_create(key, MAX_PATH, filetype_classname, L"\\shell\\LFW - Edit");
	shellext_store(HKEY_CLASSES_ROOT, key, NULL, L"LFW - Edit");

	shellext_create(key, MAX_PATH,
		filetype_classname, L"\\shell\\LFW - Edit\\command"
	);
	shellext_create(value, MAX_PATH, L"\"", path, L"\" edit \"%1\"");
	shellext_store(HKEY_CLASSES_ROOT, key, NULL, value);

	// create Get shell extension
	shellext_create(key, MAX_PATH, filetype_classname, L"\\shell\\LFW - Get");
	shellext_store(HKEY_CLASSES_ROOT, key, NULL, L"LFW - Get");

	shellext_create(key, MAX_PATH,
		filetype_classname, L"\\shell\\LFW - Get\\command"
	);
	shellext_create(value, MAX_PATH, L"\"", path, L"\" get \"%1\"");
	shellext_store(HKEY_CLASSES_ROOT, key, NULL, value);

	// create Put shell extension
	shellext_create(key, MAX_PATH, filetype_classname, L"\\shell\\LFW - Put");
	shellext_store(HKEY_CLASSES_ROOT, key, NULL, L"LFW - Put");

	shellext_create(key, MAX_PATH,
		filetype_classname, L"\\shell\\LFW - Put\\command"
	);
	shellext_create(value, MAX_PATH, L"\"", path, L"\" put \"%1\"");
	shellext_store(HKEY_CLASSES_ROOT, key, NULL, value);

	// free() allocated string
	free(filetype_classname);
}

// }}}

// uninstall all shell extensions for filetype {{{

void shellext_uninstall(const char* file_ext){
	shellext_strtolower((char*) file_ext);

	TCHAR* filetype_classname;
	TCHAR  key[MAX_PATH];
	TCHAR  wfile_ext[MAX_PATH];

	// wide file extension
	MultiByteToWideChar(
		CP_ACP, MB_PRECOMPOSED,
		file_ext, strlen(file_ext)+1,
		wfile_ext, MAX_PATH
	);

	// get full filetype class name from registry
	shellext_create(key, MAX_PATH, L".", wfile_ext);
	filetype_classname=shellext_query(HKEY_CLASSES_ROOT, key, NULL);

	// delete Checkin shell extension
	shellext_create(
		key, MAX_PATH, filetype_classname, L"\\shell\\LFW - Checkin"
	);
	RegDeleteKey(HKEY_CLASSES_ROOT, key);

	// delete Checkout shell extension
	shellext_create(
		key, MAX_PATH, filetype_classname, L"\\shell\\LFW - Checkout"
	);
	RegDeleteKey(HKEY_CLASSES_ROOT, key);

	// delete Edit shell extension
	shellext_create(key, MAX_PATH, filetype_classname, L"\\shell\\LFW - Edit");
	RegDeleteKey(HKEY_CLASSES_ROOT, key);

	// delete Get shell extension
	shellext_create(key, MAX_PATH, filetype_classname, L"\\shell\\LFW - Get");
	RegDeleteKey(HKEY_CLASSES_ROOT, key);

	// delete Put shell extension
	shellext_create(key, MAX_PATH, filetype_classname, L"\\shell\\LFW - Put");
	RegDeleteKey(HKEY_CLASSES_ROOT, key);
}

// }}}

// install all shell extensions for directory {{{

void shellext_dirinstall(){
	TCHAR buf[MAX_PATH];
	TCHAR path[MAX_PATH];

	// path to executable
	GetModuleFileName(NULL, path, MAX_PATH);

	// create directory shell extension
	shellext_create(buf, MAX_PATH, L"\"", path, L"\" checkin_dir \"%1\"");
	shellext_store(
		HKEY_CLASSES_ROOT,
		L"Directory\\shell\\LFW - Checkin Directory\\command",
		NULL, buf
	);
}

// }}}

// uninstall all shell extensions for directory {{{

void shellext_diruninstall(){
	// delete directory shell extension
	RegDeleteKey(
		HKEY_CLASSES_ROOT,
		L"Directory\\shell\\LFW - Checkin Directory\\command"
	);
}

// }}}
