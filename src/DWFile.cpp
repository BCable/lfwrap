#include "DWFile.hpp"

#include <fstream>
// strlen()
#include <stdio.h>


#ifdef LFW_WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

#include "lfw_file_compare.hpp"
#include "lfw_file_copy.hpp"
#include "lfw_file_exists.hpp"
#include "lfw_file_modtime.hpp"
#include "lfw_is_directory.hpp"
#include "lfw_string.hpp"

#ifndef LFW_CASE_SENSITIVE
#define FILENAME lowerfilename
#else
#define FILENAME filename
#endif

using namespace std;

// constructor {{{
#include <iostream>
DWFile::DWFile(Config* lc, SiteConfig* sc, string filename){
	this->lc=lc;
	this->sc=sc;

	this->filename=filename;
#ifndef LFW_CASE_SENSITIVE
	this->lowerfilename=string_lower(filename);
#endif

	if(!file_exists(filename)){
		error=FILENOTFOUND;
		return;
	}

	if(is_directory(filename)){
		error=ISDIRECTORY;
		return;
	}

	createFilesVector();

	owner_name="";
	owner_email="";
	checkedOut=alreadyCheckedOut();

	error=ERRORNONE;
}

// }}}

// private {{{

bool DWFile::alreadyCheckedOut(){
	vector<string>::size_type i;
	bool                      locked=false;
	for(i=0; i<files.size(); i++){
		locked=hasLockFile(files[i]);
		if(locked){
			parseLockFile(files[i]+LFW_LOCKFILE_SUFFIX);
			break;
		}
	}
	return locked;
}

void DWFile::createFilesVector(){
	vector<string>::size_type i;
#ifndef LFW_CASE_SENSITIVE
	string                    lowernewfilename;
#endif
	string                    newfilename;
	string::size_type         offset;
	bool                      origfile=false;
	string::size_type         pos;
	vector<string>            sites;

	offset=FILENAME.length()-1;
	pos=FILENAME.rfind(LFW_FILESYSTEM_SLASH, offset);
	while(offset!=string::npos && pos!=string::npos){
		sites=sc->getSiteLinks(FILENAME.substr(0, pos));
		for(i=0; i<sites.size(); i++){
			newfilename=sites[i]+filename.substr(pos, filename.length());
#ifndef LFW_CASE_SENSITIVE
			lowernewfilename=string_lower(newfilename);
			if(lowernewfilename.compare(FILENAME)==0)
#else
			if(newfilename.compare(FILENAME)==0)
#endif
				origfile=true;
			files.push_back(newfilename);
		}
		offset=pos-1;
		pos=FILENAME.rfind(LFW_FILESYSTEM_SLASH, offset);
	}
	if(!origfile)
		files.push_back(filename);
}

void DWFile::createLockFile(string filename){
	FILE*       fp;
	const char* out;
	string      write;

	fp=fopen((filename+LFW_LOCKFILE_SUFFIX).c_str(), "w");

	if(fp!=NULL){
		write=
			lc->getValueOf("LFW_USER")+
			LFW_LOCKFILE_DELIM+
			lc->getValueOf("LFW_EMAIL");

		out=write.c_str();

		fwrite(out, 1, write.length(), fp);
		fclose(fp);
	}
}

void DWFile::fileGet(bool force){
	time_t                    curModTime;
	string                    getFile="";
	vector<string>::size_type i;
	time_t                    recentModTime;

	if(force)
		recentModTime=0;
	else{
		recentModTime=file_modtime(FILENAME);
	}

	for(i=0; i<files.size(); i++){
		if(files[i].compare(FILENAME)!=0){
			curModTime=file_modtime(files[i]);
			if(curModTime>recentModTime){
				recentModTime=curModTime;
				getFile=files[i];
			}
		}
	}

	if(getFile.compare("")==0)
		error=MODTIMEERROR;
	else{
		file_copy(getFile, filename);
		error=ERRORNONE;
	}
}

void DWFile::filePut(bool force){
	time_t                    curModTime;
	vector<string>::size_type i;
	time_t                    localModTime;

	localModTime=file_modtime(FILENAME);

	// first check to see if any of the files are over the modified date
	if(!force){
		error=ERRORNONE;
		for(i=0; i<files.size(); i++){
			if(files[i].compare(FILENAME)!=0){
				curModTime=file_modtime(files[i]);
				if(curModTime>localModTime && !file_compare(files[i],FILENAME)){
					error=MODTIMEERROR;
					return;
				}
			}
		}
	}

	// now do the actual putting
	for(i=0; i<files.size(); i++){
		if(files[i].compare(FILENAME)!=0){
			file_copy(FILENAME, files[i]);
		}
	}
}

bool DWFile::hasLockFile(string filename){
	return file_exists(filename+LFW_LOCKFILE_SUFFIX);
}

void DWFile::parseLockFile(string lockfile){
	char*             buf;
	FILE*             fp;
	long              size;
	string::size_type split;

	fp=fopen(lockfile.c_str(), "r");

	fseek(fp, 0, SEEK_END);
	size=ftell(fp);
	rewind(fp);

	buf=(char*) malloc(sizeof(char)*(size+1));
	buf[size]='\0';
	fread(buf, 1, size, fp);
	string fcont (buf);

	split=fcont.find(LFW_LOCKFILE_DELIM);
	if(split==string::npos){
		owner_name=fcont;
		owner_email=fcont;
	}
	else{
		fcont=string_strip(fcont);
		owner_name=fcont.substr(0, split);
		owner_email=fcont.substr(
			split+strlen(LFW_LOCKFILE_DELIM),
			fcont.length()-split-strlen(LFW_LOCKFILE_DELIM)
		);
	}

	fclose(fp);
}

#ifdef LFW_GNU
void DWFile::removeLockFile(string filename){
	unlink((filename+LFW_LOCKFILE_SUFFIX).c_str());
}
#else
void DWFile::removeLockFile(string filename){
	_unlink((filename+LFW_LOCKFILE_SUFFIX).c_str());
}
#endif

// }}}

// public {{{

void DWFile::checkIn(){
	vector<string>::size_type i;
	for(i=0; i<files.size(); i++){
		if(hasLockFile(files[i]))
			removeLockFile(files[i]);
	}
	checkedOut=false;
}

void DWFile::checkOut(){
	vector<string>::size_type i;
	for(i=0; i<files.size(); i++){
		createLockFile(files[i]);
	}
	checkedOut=true;
}

vector<string> DWFile::getAllFiles(){
	return files;
}

short DWFile::getError(){
	return error;
}

string DWFile::getFilename(){
	return filename;
}

string DWFile::getOwnerEmail(){
	return owner_email;
}

string DWFile::getOwnerName(){
	return owner_name;
}

bool DWFile::isCheckedOut(){
	return checkedOut;
}

// }}}
