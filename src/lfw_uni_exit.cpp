#include "lfw_uni_exit.hpp"

#include "globals.hpp"

#include <stdlib.h>

void uni_exit(int error, bool doPause){
#ifdef LFW_WIN32
	if(doPause)
		system("pause");
#endif
	exit(error);
}

void uni_exit(int error){
	uni_exit(error, true);
}
