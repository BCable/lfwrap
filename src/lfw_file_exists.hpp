#ifndef _lfwrap_file_exists_hpp
#define _lfwrap_file_exists_hpp

#include <string>

bool file_exists(std::string);

#endif
