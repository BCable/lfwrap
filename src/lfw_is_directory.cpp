#include "lfw_is_directory.hpp"

#include "globals.hpp"

#include <sys/types.h>
#include <sys/stat.h>

using namespace std;

#ifdef LFW_GNU
bool is_directory(string filename){
	struct stat info;

	if(stat(filename.c_str(), &info)!=0)
		return false;

	if(S_ISDIR(info.st_mode))
		return true;
	else
		return false;
}
#else
bool is_directory(string filename){
	struct _stat64i32 info;

	if(_stat(filename.c_str(), &info)!=0)
		return false;

	if(info.st_mode & _S_IFDIR)
		return true;
	else
		return false;
}
#endif
