#include "SiteConfig.hpp"

#include <fstream>

#include "lfw_string.hpp"

using namespace std;

// constructors {{{

SiteConfig::SiteConfig(const char* filename){
	ifstream config_file (filename);
	string line;
	int splitter;
	string sitename;
	string working_line;

	this->combos=0;

	if(!config_file.is_open()){
		error=FILENOTFOUND;
		return;
	}

	while(!config_file.eof()){
		getline(config_file, line);

		// skip commented lines
		if(line.length()>0 && line.at(0)=='#')
			continue;

#ifndef LFW_CASE_SENSITIVE
		line=string_lower(line);
#endif
		working_line=line;

		while(true){
			splitter=working_line.find_first_of('=');
			if(splitter==-1 && line.compare(working_line)==0)
				break;

			sitename=string_strip(working_line.substr(0,splitter));

			sites_name.push_back(sitename);
			sites_value.push_back(this->combos);

			working_line=string_strip(working_line.substr(splitter+1));

			if(splitter==-1)
				break;
		}

		this->combos++;
	}

	error=ERRORNONE;
}

// }}}

// public {{{

short SiteConfig::getError(){
	return error;
}

vector<string> SiteConfig::getSiteLinks(string sitename){
	vector<string>::size_type i;
	int site=-1;
	vector<string> ret;
	sitename=string_lower(sitename);

	for(i=0; i<sites_name.size(); i++){
		if(sitename.compare(sites_name.at(i))==0){
			site=sites_value.at(i);
			break;
		}
	}

	if(site==-1)
		return ret;

	for(i=0; i<sites_name.size(); i++){
		if(sites_value.at(i)==site){
			ret.push_back(sites_name.at(i));
		}
	}

	return ret;
}

// }}}
