#ifndef _lfwrap_edit_file_hpp_
#define _lfwrap_edit_file_hpp_

#include <string>

#include "Config.hpp"

void edit_file(Config*, std::string);

#endif
