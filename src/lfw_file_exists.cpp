#include "lfw_file_exists.hpp"

#include "globals.hpp"

#include <sys/types.h>
#include <sys/stat.h>

using namespace std;

#ifdef LFW_GNU
bool file_exists(string filename){
	struct stat info;

	if(stat(filename.c_str(), &info)==0)
		return true;
	else
		return false;
}
#else
bool file_exists(string filename){
	struct _stat64i32 info;

	if(_stat(filename.c_str(), &info)==0)
		return true;
	else
		return false;
}
#endif
