#ifndef _lfwrap_cmd_checkin_dir_hpp
#define _lfwrap_cmd_checkin_dir_hpp

#include "Config.hpp"
#include "SiteConfig.hpp"

#include <string>

void cmd_checkin_dir(Config*, SiteConfig*, std::string);

#endif
