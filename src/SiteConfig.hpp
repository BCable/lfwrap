#ifndef _lfwrap_siteconfig_hpp_
#define _lfwrap_siteconfig_hpp_

#include <string>
#include <vector>

class SiteConfig {
	private:
		int                      combos;
		short                    error;
		std::vector<std::string> sites_name;
		std::vector<int>         sites_value;
	public:
		SiteConfig(const char*);

		// errors
		static const short ERRORNONE=0;
		static const short FILENOTFOUND=1;

		short                    getError();
		std::vector<std::string> getSiteLinks(std::string);
};

#endif
