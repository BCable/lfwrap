#ifndef _lfwrap_dwfile_hpp_
#define _lfwrap_dwfile_hpp_

#include <string>
#include <vector>

#include "globals.hpp"
#include "Config.hpp"
#include "SiteConfig.hpp"

class DWFile {
	private:
		bool                     checkedOut;
		short                    error;
		std::string              filename;
		std::vector<std::string> files;
		Config*                  lc;
#ifndef LFW_CASE_SENSITIVE
		std::string              lowerfilename;
#endif
		std::string              owner_email;
		std::string              owner_name;
		SiteConfig*              sc;

		bool alreadyCheckedOut();
		void createFilesVector();
		void createLockFile(std::string);
		bool hasLockFile(std::string);
		void parseLockFile(std::string);
		void removeLockFile(std::string);

	public:
		DWFile(Config*, SiteConfig*, std::string);

		// errors
		static const short ERRORNONE=0;
		static const short FILENOTFOUND=1;
		static const short ISDIRECTORY=2;
		static const short MODTIMEERROR=3;

		void                     checkIn();
		void                     checkOut();
		void                     fileGet(bool);
		void                     filePut(bool);
		std::vector<std::string> getAllFiles();
		short                    getError();
		std::string              getFilename();
		std::string              getOwnerEmail();
		std::string              getOwnerName();
		bool                     isCheckedOut();
};

#endif
