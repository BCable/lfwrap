#ifndef _lfwrap_shellext_hpp_
#define _lfwrap_shellext_hpp_

void shellext_install(const char*);
void shellext_uninstall(const char*);
void shellext_dirinstall();
void shellext_diruninstall();

#endif
