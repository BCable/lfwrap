#include "globals.hpp"

#include <iostream>

#include "Config.hpp"
#include "SiteConfig.hpp"
#include "DWFile.hpp"

#include "lfw_cmd_checkin.hpp"
#include "lfw_cmd_checkin_dir.hpp"
#include "lfw_edit_file.hpp"
#include "lfw_string.hpp"
#include "lfw_uni_exit.hpp"

#ifdef LFW_WIN32
#include "lfw_shellext.hpp"
#endif

#if LFW_CONFIG_TYPE==LFW_RELATIVE_APP
#include <string.h>
#endif

using namespace std;

int main(int argc, char* argv[]){
	Config* lc;
	SiteConfig* sc;
	DWFile* dwf;
	vector<string> sites;

	if(argc<3 && (argc<2 || (
#ifndef LFW_WIN32
		strcmp(argv[1],"install_shell_dir")!=0 &&
		strcmp(argv[1],"uninstall_shell_dir")!=0 &&
#endif
		strcmp(argv[1],"-v")!=0
	))){
		cerr << "Not enough arguments." << endl;
		uni_exit(1);
	}

	// version command
	if(strcmp(argv[1],"-v")==0){
		cout << "LFWrap " << VERSION << endl;
		cout << "Author: Brad Cable" << endl;
		cout << "http://bcable.net/project.php?lfwrap" << endl;
		uni_exit(0, false);
	}

	// lfwrap.conf
#if LFW_CONFIG_TYPE==LFW_RELATIVE_APP
	string config=argv[0];
	config=config.substr(0, config.find_last_of(LFW_FILESYSTEM_SLASH)+1);
	if(config.compare("./")==0)
		config=LFW_CONFIG_FILE;
	else
		config+=LFW_CONFIG_FILE;
	lc=new Config(config);
#else
	lc=new Config(LFW_CONFIG_FILE);
#endif

	// parse errors
	switch(lc->getError()){
		case Config::ERRORNONE:
			break;

		case Config::FILENOTFOUND:
			cerr << "Config: " << MSG_ERROR_FILENOTFOUND << endl;
			cerr << endl;
			cerr << "Make sure lfwrap.conf exists in the same directory as the";
			cerr << " binary." << endl;
			uni_exit(1);
			break;

		default:
			cerr << "Config: " << MSG_ERROR_UNKNOWN << endl;
			uni_exit(1);
			break;
	}

#ifdef LFW_WIN32
	// install_shell command
	if(strcmp(argv[1],"install_shell")==0){
		shellext_install(argv[2]);
		cout << "Shell extensions installed for files of type '";
		cout << argv[2] << "'." << endl;
		uni_exit(0, false);
	}

	// uninstall_shell command
	else if(strcmp(argv[1],"uninstall_shell")==0){
		shellext_uninstall(argv[2]);
		cout << "Shell extensions uninstalled for files of type '";
		cout << argv[2] << "'." << endl;
		uni_exit(0, false);
	}

	// install_shell_dir command
	else if(strcmp(argv[1],"install_shell_dir")==0){
		shellext_dirinstall();
		cout << "Shell extensions installed for all directories." << endl;
		uni_exit(0, false);
	}

	// uninstall_shell_dir command
	else if(strcmp(argv[1],"uninstall_shell_dir")==0){
		shellext_diruninstall();
		cout << "Shell extensions uninstalled for all directories." << endl;
		uni_exit(0, false);
	}
#endif

	// sites.conf
	sc=new SiteConfig((
		lc->getValueOf("LFW_CONFIG_DIR")+
		LFW_FILESYSTEM_SLASH+
		lc->getValueOf("LFW_CONFIG_SITES")
	).c_str());

	// parse errors
	switch(sc->getError()){
		case SiteConfig::ERRORNONE:
			break;

		case SiteConfig::FILENOTFOUND:
			cerr << "Site config: " << MSG_ERROR_FILENOTFOUND << endl;
			cerr << endl;
			cerr << "Make sure you have edited lfwrap.conf and sites.conf";
			cerr << " exists." << endl;
			uni_exit(1);
			break;

		default:
			cerr << "Site config: " << MSG_ERROR_UNKNOWN << endl;
			uni_exit(1);
			break;
	}

	// create DWFile
	string filename (argv[2]);
	dwf=new DWFile(lc, sc, filename);

	// parse errors
	switch(dwf->getError()){
		case DWFile::ERRORNONE:
			break;

		case DWFile::FILENOTFOUND:
			cerr << "User file: " << MSG_ERROR_FILENOTFOUND << endl;
			uni_exit(1);
			break;

		case DWFile::ISDIRECTORY:
			if(strcmp(argv[1],"checkin_dir")==0)
				break;
			cerr << "User file: " << MSG_ERROR_ISDIRECTORY << endl;
			uni_exit(1);
			break;

		default:
			cerr << "User file: " << MSG_ERROR_UNKNOWN << endl;
			uni_exit(1);
			break;
	}

	// stat command
	if(strcmp(argv[1],"stat")==0){
		vector<string>            files;
		vector<string>::size_type i;

		cout << "File: " << filename << endl;

		cout << "Checked Out: ";
		if(dwf->isCheckedOut()){
			cout << "Yes" << endl;
			cout << "Owner Name: " << dwf->getOwnerName() << endl;
			cout << "Owner Email: " << dwf->getOwnerEmail() << endl;
		}
		else
			cout << "No" << endl;

		cout << endl;
		cout << "All Associated Files:" << endl;

		files=dwf->getAllFiles();
		for(i=0; i<files.size(); i++){
			cout << "  " << files[i] << endl;
		}
	}

	// checkin command
	else if(strcmp(argv[1],"checkin")==0){
		cmd_checkin(lc, dwf);
	}

	// checkout/edit commands
	else if(strcmp(argv[1],"checkout")==0 || strcmp(argv[1],"edit")==0){
		if(dwf->isCheckedOut()){
			if(dwf->getOwnerName().compare(lc->getValueOf("LFW_USER"))!=0){
				cout << "File is checked out by '" << dwf->getOwnerName();
				cout << "', override checkout? ";

				char c;
				cin >> c;
				if(c!='y' && c!='Y'){
					cout << "Quitting." << endl;
					uni_exit(0);
				}

				dwf->checkIn();
				dwf->checkOut();
			}
			else
				cout << "File is already checked out." << endl;
		}
		else
			dwf->checkOut();

		// edit command
		if(strcmp(argv[1],"edit")==0)
			edit_file(lc, filename);
	}

	// get command
	else if(strcmp(argv[1],"get")==0){
		cout << "This will overwrite the local copy with the most recent";
		cout << " remote copy." << endl;
		cout << "Are you sure you wish to proceed? ";

		char c;
		cin >> c;
		if(c!='y' && c!='Y'){
			cout << "Quitting." << endl;
			uni_exit(0);
		}

		dwf->fileGet(false);

		switch(dwf->getError()){
			case DWFile::ERRORNONE:
				break;

			case DWFile::MODTIMEERROR:
				cout << endl;
				cout << "The local file is more recent than all of the remote";
				cout << " copies." << endl << "Are you sure you wish to get";
				cout << " the most recent remote copy? ";

				cin >> c;
				if(c!='y' && c!='Y'){
					cout << "Quitting." << endl;
					uni_exit(0);
				}

				dwf->fileGet(true);
				break;
		}
	}

	// put command
	else if(strcmp(argv[1],"put")==0){
		cout << "This will overwrite all remote copies with the local copy.";
		cout << endl;
		cout << "Are you sure you wish to proceed? ";

		char c;
		cin >> c;
		if(c!='y' && c!='Y'){
			cout << "Quitting." << endl;
			uni_exit(0);
		}

		dwf->filePut(false);

		switch(dwf->getError()){
			case DWFile::ERRORNONE:
				break;

			case DWFile::MODTIMEERROR:
				cout << endl;
				cout << "At least one remote file is more recent than the";
				cout << " local file." << endl;
				cout << "Are you sure you wish to overwrite all remote files? ";

				cin >> c;
				if(c!='y' && c!='Y'){
					cout << "Quitting." << endl;
					uni_exit(0);
				}

				dwf->filePut(true);
				break;
		}
	}

	// checkin_dir command
	else if(strcmp(argv[1],"checkin_dir")==0){
		cmd_checkin_dir(lc, sc, filename);
	}

	else{
		cerr << "Unknown command." << endl;
		uni_exit(1);
	}

	return 0;
}
