#include "lfw_cmd_checkin_dir.hpp"

#include "globals.hpp"

#include "DWFile.hpp"

#include "lfw_cmd_checkin.hpp"
#include "lfw_is_directory.hpp"
#include "lfw_string.hpp"
#include "lfw_uni_exit.hpp"

#include <iostream>

#include <sys/types.h>
#include <errno.h>

#ifndef LFW_WIN32
#include <dirent.h>
#else
#include <windows.h>
#endif


using namespace std;

void checkin_file(
	Config* lc, SiteConfig* sc, string curFile, bool* noToAll, bool* yesToAll
){
	bool doCheckIn=true;
	DWFile dwf (lc, sc, curFile);

	if(dwf.isCheckedOut()){
		if(dwf.getOwnerName().compare(lc->getValueOf("LFW_USER"))!=0){
			if(*noToAll)
				doCheckIn=false;

			else if(!(*yesToAll)){
				cout << "File is checked out by '";
				cout << dwf.getOwnerName();
				cout << "', checkin anyway?" << endl;
				cout << "[Y]es, [N]o, Yes to [A]ll, N[O] to All: ";

				char c;
				cin >> c;
				if(c=='a' || c=='A')
					*yesToAll=true;
				else if(c=='o' || c=='O'){
					*noToAll=true;
					doCheckIn=false;
				}
				else if(c!='y' && c!='Y'){
					cout << "Skipping..." << endl;
					doCheckIn=false;
				}

				cout << endl;
			}
		}
	}
	if(doCheckIn)
		dwf.checkIn();
}

#ifndef LFW_WIN32
void cmd_checkin_dir(Config* lc, SiteConfig* sc, string dirname){
	string         curFile;
	string         curFileLower;
	struct dirent* d;
    DIR*           dir;
	bool           noToAll=false;
	bool           yesToAll=false;

	errno=0;
	dir=opendir(dirname.c_str());
	if(errno!=0){
		cerr << "An error occurred trying to open the directory structure.";
		cerr << endl;
		cerr << errno << endl;
		uni_exit(0);
	}

	while((d=readdir(dir))!=NULL){
		if(strcmp(d->d_name, ".")==0) continue;
		if(strcmp(d->d_name, "..")==0) continue;

		curFile=dirname+LFW_FILESYSTEM_SLASH+d->d_name;
		curFileLower=string_lower(curFile.substr(curFile.length()-4));
		if(curFileLower.compare(".lck")==0) continue;

		if(is_directory(curFile))
			cmd_checkin_dir(lc, sc, curFile);
		else
			checkin_file(lc, sc, curFile, &noToAll, &yesToAll);
	}

	closedir(dir);
}
#else
void cmd_checkin_dir(Config* lc, SiteConfig* sc, string dirname){
	char*           cstr;
	string          curFile;
	string          curFileLower;
	WIN32_FIND_DATA d;
    HANDLE          dir;
	bool            noToAll=false;
	wchar_t*        wcstr;
	bool            yesToAll=false;

	// convert to wchar_t from char (and append "\*" to resulting string)
	cstr=(char*) dirname.c_str();
	wcstr=(wchar_t*) malloc(sizeof(wchar_t)*(strlen(cstr)+2+1));
	wcstr[strlen(cstr)]='\\';
	wcstr[strlen(cstr)+1]='*';
	wcstr[strlen(cstr)+2]=0;
	use_facet<ctype<wchar_t>>(locale()).widen(cstr,cstr+strlen(cstr),wcstr);

	dir=FindFirstFile(wcstr, &d);
	free(wcstr);

	if(dir==INVALID_HANDLE_VALUE){
		cerr << "An error occurred trying to open the directory structure.";
		cerr << endl;
		uni_exit(0);
	}

	while(FindNextFile(dir, &d)){
		if(wcscmp(d.cFileName, TEXT("."))==0) continue;
		if(wcscmp(d.cFileName, TEXT(".."))==0) continue;

		// convert to char from wchar_t
		wcstr=d.cFileName;
		cstr=(char*) malloc(sizeof(char)*(wcslen(wcstr)+1));
		cstr[wcslen(wcstr)]=0;
		use_facet<ctype<wchar_t>>(locale()).narrow(
			wcstr,wcstr+wcslen(wcstr),'?',cstr
		);

		curFile=dirname+LFW_FILESYSTEM_SLASH+cstr;
		free(cstr);
		curFileLower=string_lower(curFile.substr(curFile.length()-4));
		if(curFileLower.compare(".lck")==0) continue;

		if(is_directory(curFile))
			cmd_checkin_dir(lc, sc, curFile);
		else
			checkin_file(lc, sc, curFile, &noToAll, &yesToAll);
	}

	FindClose(dir);
}
#endif
